use regex::Captures;
use eyre::Result;
use super::*;

pub struct HasEpisodeName {}
impl Rule for HasEpisodeName {
    fn check(_filename: &str, captures: &Captures) -> Option<NonCompliantReason> {
        if captures.name("name").is_none() {
            return Some(NonCompliantReason::MissingName)
        }

        return None
    }

    fn fix(_filename: &str, _captures: &Captures) -> Result<FixStatus> {
        return Ok(FixStatus::NotImplemented)
    }
}

#[cfg(test)]
mod tests {
    use crate::rules::{NonCompliantReason, Rule};
    use crate::SERIES_REGEX;

    #[test]
    fn has_name() {
        let title = "Test S00E00 - FooBar.mkv";
        let captures = regex::Regex::new(SERIES_REGEX).unwrap().captures(title).unwrap();
        assert_eq!(super::HasEpisodeName::check(title, &captures), None);
    }

    #[test]
    fn no_name() {
        let title = "Test S00E00.mkv";
        let captures = regex::Regex::new(SERIES_REGEX).unwrap().captures(title).unwrap();
        assert_eq!(super::HasEpisodeName::check(title, &captures), Some(NonCompliantReason::MissingName));
    }
}