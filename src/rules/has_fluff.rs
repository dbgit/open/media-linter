use regex::Captures;
use eyre::Result;
use super::*;

const KNOWN_FLUFF: [&str; 7] = ["[", "xvid", "BluRay", "1080p", "264", "dvdrip", "web-dl"];

pub struct HasFluff {}
impl Rule for HasFluff {
    fn check(filename: &str, _captures: &Captures) -> Option<NonCompliantReason> {
        let lowered_filename = filename.to_lowercase();
        for fluff in KNOWN_FLUFF {
            if lowered_filename.contains(fluff) {
                return Some(NonCompliantReason::HasFluff)
            }
        }

        return None
    }

    fn fix(_filename: &str, _captures: &Captures) -> Result<FixStatus> {
        return Ok(FixStatus::NotFixable)
    }
}

#[cfg(test)]
mod tests {
    use crate::rules::{NonCompliantReason, Rule};

    #[test]
    fn no_fluff() {
        let captures = regex::Regex::new("").unwrap().captures("").unwrap();
        assert_eq!(super::HasFluff::check("Test S00E00 - FooBar.mkv", &captures), None);
    }

    #[test]
    fn encoding_info() {
        let captures = regex::Regex::new("").unwrap().captures("").unwrap();
        let title = "blah (1920x1080 h264 BD FLAC).mkv";
        assert_eq!(super::HasFluff::check(title, &captures), Some(NonCompliantReason::HasFluff));
    }
}