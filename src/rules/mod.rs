use std::fmt::{Display, Formatter};
use regex::Captures;
use eyre::Result;

mod episode_marker;
mod has_fluff;
mod has_dash_in_title;
mod has_episode_name;
mod missing_name_separator;

pub use episode_marker::EpisodeMarker;
pub use has_fluff::HasFluff;
pub use has_episode_name::HasEpisodeName;
pub use has_dash_in_title::DashInTitle;
pub use missing_name_separator::MissingNameSeparator;

pub const SERIES_RULES_WITHOUT_NAME: [fn(&str, &Captures) -> Option<NonCompliantReason>; 3] = [
    DashInTitle::check,
    EpisodeMarker::check,
    HasFluff::check,
];

pub const SERIES_RULES_WITH_NAME: [fn(&str, &Captures) -> Option<NonCompliantReason>; 5] = [
    DashInTitle::check,
    EpisodeMarker::check,
    HasFluff::check,
    HasEpisodeName::check,
    MissingNameSeparator::check
];

pub const MOVIE_RULES: [fn(&str, &Captures) -> Option<NonCompliantReason>; 1] = [
    HasFluff::check,
];

#[derive(Debug, PartialEq)]
pub enum NonCompliantReason {
    DashInTitle,
    MissingName,
    HasFluff,
    MissingNameSeparator,
    InvalidEpisodeMarker(EpisodeMarkerReason)
}

impl Display for NonCompliantReason {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            NonCompliantReason::DashInTitle => write!(f, "Title contains dash"),
            NonCompliantReason::MissingName => write!(f, "Missing episode name"),
            NonCompliantReason::HasFluff => write!(f, "Has fluff"),
            NonCompliantReason::MissingNameSeparator => write!(f, "Missing episode name separator"),
            NonCompliantReason::InvalidEpisodeMarker(r) => write!(f, "Episode marker must be of format S00E00 - {}", r)
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum EpisodeMarkerReason {
    SeasonPrefix,
    MissingSeasonPrefix,
    EpisodePrefix,
    MissingEpisodePrefix,
    SeasonDigits,
    MissingSeasonDigits,
    EpisodeDigits,
    MissingEpisodeDigits
}

impl Display for EpisodeMarkerReason {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            EpisodeMarkerReason::SeasonPrefix => write!(f, "Season Prefix must be 'S'"),
            EpisodeMarkerReason::MissingSeasonPrefix => write!(f, "No Season Prefix found"),
            EpisodeMarkerReason::EpisodePrefix => write!(f, "Episode Prefix must be 'E'"),
            EpisodeMarkerReason::MissingEpisodePrefix => write!(f, "No Episode Prefix found"),
            EpisodeMarkerReason::SeasonDigits => write!(f, "Season should be annotated with either 2 or 3 digits"),
            EpisodeMarkerReason::MissingSeasonDigits => write!(f, "No Season digits found"),
            EpisodeMarkerReason::EpisodeDigits => write!(f, "Episode should be annotated with either 2 or 3 digits"),
            EpisodeMarkerReason::MissingEpisodeDigits => write!(f, "No Episode digits found"),
        }
    }
}

pub enum DetectedFileType {
    SeriesEpisode,
    Movie
}

pub enum ComplianceStatus {
    NonCompliant(DetectedFileType, NonCompliantReason),
    NotMatched,
    Compliant(DetectedFileType),
    Ignored
}

pub enum FixStatus {
    Fixed(String),
    NotImplemented,
    NotFixable
}

pub trait Rule {
    fn check(filename: &str, captures: &Captures) -> Option<NonCompliantReason>;
    fn fix(filename: &str, captures: &Captures) -> Result<FixStatus>;
}
