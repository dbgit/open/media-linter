use regex::Captures;
use eyre::Result;
use super::*;

pub struct EpisodeMarker {}
impl Rule for EpisodeMarker {
    fn check(_filename: &str, captures: &Captures) -> Option<NonCompliantReason> {
        match captures.name("seasonPrefix") {
            Some(marker) => {
                if marker.as_str() != "S" {
                    return Some(NonCompliantReason::InvalidEpisodeMarker(EpisodeMarkerReason::SeasonPrefix))
                }
            }
            None => return Some(NonCompliantReason::InvalidEpisodeMarker(EpisodeMarkerReason::MissingSeasonPrefix)),
        }

        match captures.name("episodePrefix") {
            Some(marker) => {
                if marker.as_str() != "E" {
                    return Some(NonCompliantReason::InvalidEpisodeMarker(EpisodeMarkerReason::EpisodePrefix))
                }
            }
            None => return Some(NonCompliantReason::InvalidEpisodeMarker(EpisodeMarkerReason::MissingEpisodePrefix)),
        }

        match captures.name("season") {
            Some(marker) => {
                let str = marker.as_str();
                if str.len() != 2 && str.len() != 3 {
                    return Some(NonCompliantReason::InvalidEpisodeMarker(EpisodeMarkerReason::SeasonDigits))
                }
            }
            None => return Some(NonCompliantReason::InvalidEpisodeMarker(EpisodeMarkerReason::MissingSeasonDigits)),
        }

        match captures.name("episode") {
            Some(marker) => {
                let str = marker.as_str();
                if str.len() != 2 && str.len() != 3 {
                    return Some(NonCompliantReason::InvalidEpisodeMarker(EpisodeMarkerReason::EpisodeDigits))
                }
            }
            None => return Some(NonCompliantReason::InvalidEpisodeMarker(EpisodeMarkerReason::MissingEpisodeDigits)),
        }

        return None
    }

    fn fix(_filename: &str, _captures: &Captures) -> Result<FixStatus> {
        return Ok(FixStatus::NotImplemented)
    }
}

#[cfg(test)]
mod tests {
    use crate::rules::{NonCompliantReason, EpisodeMarkerReason, Rule};
    use crate::SERIES_REGEX;

    #[test]
    fn has_dash() {
        let title = "Test S00E00 - FooBar.mkv";
        let captures = regex::Regex::new(SERIES_REGEX).unwrap().captures(title).unwrap();
        assert_eq!(super::EpisodeMarker::check(title, &captures), None);
    }

    #[test]
    fn no_dash() {
        let title = "Test S00x00.mkv";
        let captures = regex::Regex::new(SERIES_REGEX).unwrap().captures(title).unwrap();
        assert_eq!(super::EpisodeMarker::check(title, &captures), Some(NonCompliantReason::InvalidEpisodeMarker(EpisodeMarkerReason::EpisodePrefix)));
    }
}