use regex::Captures;
use eyre::Result;
use super::*;
use crate::utils::constructors::episode_name;

pub struct DashInTitle {}
impl Rule for DashInTitle {
    fn check(_filename: &str, captures: &Captures) -> Option<NonCompliantReason> {
        if captures.name("titleSeparator").is_some() {
            return Some(NonCompliantReason::DashInTitle)
        }

        return None
    }

    fn fix(_filename: &str, captures: &Captures) -> Result<FixStatus> {
        let ep_name = match captures.name("name") {
            Some(ep) => Some(ep.as_str().to_string()),
            None => None
        };

        let name = episode_name(
            captures.name("title").unwrap().as_str().to_string(),
            captures.name("season").unwrap().as_str().to_string(),
            captures.name("episode").unwrap().as_str().to_string(),
            ep_name,
            captures.name("ext").unwrap().as_str().to_string()
        );

        return Ok(FixStatus::Fixed(name))
    }
}

#[cfg(test)]
mod tests {
    use crate::rules::{NonCompliantReason, Rule};
    use crate::SERIES_REGEX;

    #[test]
    fn has_dash() {
        let title = "Test S00E00 - FooBar.mkv";
        let captures = regex::Regex::new(SERIES_REGEX).unwrap().captures(title).unwrap();
        assert_eq!(super::DashInTitle::check(title, &captures), None);
    }

    #[test]
    fn no_dash() {
        let title = "Test - S00E00.mkv";
        let captures = regex::Regex::new(SERIES_REGEX).unwrap().captures(title).unwrap();
        assert_eq!(super::DashInTitle::check(title, &captures), Some(NonCompliantReason::DashInTitle));
    }
}