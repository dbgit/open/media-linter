use regex::Captures;
use eyre::Result;
use super::*;

pub struct MissingNameSeparator {}
impl Rule for MissingNameSeparator {
    fn check(_filename: &str, captures: &Captures) -> Option<NonCompliantReason> {
        if captures.name("nameSeparator").is_none() {
            return Some(NonCompliantReason::MissingNameSeparator)
        }

        return None
    }

    fn fix(_filename: &str, _captures: &Captures) -> Result<FixStatus> {
        return Ok(FixStatus::NotImplemented)
    }
}

#[cfg(test)]
mod tests {
    use crate::rules::{NonCompliantReason, Rule};
    use crate::SERIES_REGEX;

    #[test]
    fn has_separator() {
        let title = "Test S00E00 - FooBar.mkv";
        let captures = regex::Regex::new(SERIES_REGEX).unwrap().captures(title).unwrap();
        assert_eq!(super::MissingNameSeparator::check(title, &captures), None);
    }

    #[test]
    fn no_separator() {
        let title = "Test S00E00 FooBar.mkv";
        let captures = regex::Regex::new(SERIES_REGEX).unwrap().captures(title).unwrap();
        assert_eq!(super::MissingNameSeparator::check(title, &captures), Some(NonCompliantReason::MissingNameSeparator));
    }
}