mod args;
mod rules;
mod utils;

use std::fmt::{Display, Formatter};
use eyre::Result;
use clap::Parser;
use lazy_static::lazy_static;
use regex::{Captures, Regex};
use walkdir::{DirEntry, WalkDir};

use args::Args;
use rules::*;
use utils::status_icon::*;

const VIDEO_EXTENSIONS: [&str; 14] = ["mkv", "mp4", "avi", "webm", "mov", "wmv", "flv", "ogg", "ogv", "yuv", "amv", "mpg", "mpeg", "m4v"];
pub(crate) const SERIES_REGEX: &str = r"^(?P<title>.*?)(?P<titleSeparator>\s-\s?)?(?P<seasonPrefix>[Ss]|\s|\.)(?P<season>\d{1,3})(?P<episodePrefix>[Ee]|[Xx]|[Ss])(?P<episode>\d{1,3})([Ee](?P<episode2>\d{2,3}))?((?P<nameSeparator>\s-\s)?(?P<name>.+))?\.(?P<ext>...)$";
pub(crate) const MOVIE_REGEX: &str = r"^(?P<title>.*?)\s(?P<year>\(\d{4}\))\s(?P<resolution>\[.+\])\.(?P<ext>...)$";

lazy_static! {
    static ref SERIES_PARSER: Regex = Regex::new(SERIES_REGEX).unwrap();
    static ref MOVIE_PARSER: Regex = Regex::new(MOVIE_REGEX).unwrap();
}

struct Stats {
    success: usize,
    warning: usize,
    error: usize,
    files: usize,
}

impl Display for Stats {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Files: {} - Success: {} - Warnings: {} - Errors: {}",
            self.files,
            self.success,
            self.warning,
            self.error)
    }
}

struct Context {
    current_folder: String,
    has_printed_folder: bool,
}

fn main() -> Result<()> {
    let params = Args::parse();

    let mut stats = Stats {
        files: 0,
        success: 0,
        warning: 0,
        error: 0,
    };

    let mut ctx = Context {
        current_folder: "".to_string(),
        has_printed_folder: false,
    };

    for entry in WalkDir::new(params.path) {
        if let Ok(file) = entry {
            check_file(file, &mut stats, &mut ctx, params.no_emoji, params.show_success, params.episode_name);
        }
    }

    println!("{}", stats);

    Ok(())
}

fn print_result(media_icon: MediaIcon, status_icon: StatusIcon, filename: &str, reason: Option<String>, no_emoji: bool) {
    match reason {
        None => println!(
            "{} {} {}",
            get_media_icon(media_icon, no_emoji),
            get_status_icon(status_icon, no_emoji),
            filename),
        Some(r) => println!(
            "{} {} {} --- ({})",
            get_media_icon(media_icon, no_emoji),
            get_status_icon(StatusIcon::Warning, no_emoji),
            filename,
            r)
    }
}

fn check_file(file: DirEntry, stats: &mut Stats, ctx: &mut Context, no_emoji: bool, show_success: bool, episode_name: bool) -> () {
    let file_type = file.file_type();
    if file_type.is_file() {
        let filename = file.file_name().to_str().unwrap();
        match lint_file_name(&file, filename, episode_name) {
            ComplianceStatus::NotMatched => {
                stats.files += 1;
                stats.error += 1;
                if !ctx.has_printed_folder {
                    println!("\n\n=== {} {}", get_status_icon(StatusIcon::Directory, no_emoji), ctx.current_folder);
                    ctx.has_printed_folder = true;
                }
                print_result(MediaIcon::Unknown, StatusIcon::Failure, filename, Some("Failed to match".to_string()), no_emoji);
            }
            ComplianceStatus::NonCompliant(detected_file_type, reason) => {
                stats.files += 1;
                if !ctx.has_printed_folder {
                    println!("\n\n=== {} {}", get_status_icon(StatusIcon::Directory, no_emoji), ctx.current_folder);
                    ctx.has_printed_folder = true;
                }
                stats.warning += 1;
                print_result(MediaIcon::from(detected_file_type), StatusIcon::Warning, filename, Some(reason.to_string()), no_emoji);
            }
            ComplianceStatus::Compliant(detected_file_type) => {
                stats.files += 1;
                stats.success += 1;
                if show_success {
                    if !ctx.has_printed_folder {
                        println!("\n\n=== {} {}", get_status_icon(StatusIcon::Directory, no_emoji), ctx.current_folder);
                        ctx.has_printed_folder = true;
                    }
                    print_result(MediaIcon::from(detected_file_type), StatusIcon::Success, filename, None, no_emoji);
                }
            }
            ComplianceStatus::Ignored => {}
        }
    } else if file_type.is_dir() {
        ctx.current_folder = file.path().display().to_string();
        ctx.has_printed_folder = false;
    }
}

enum FileType<'lt> {
    SeriesEpisode(Captures<'lt>),
    Movie(Captures<'lt>),
    Unknown
}

fn parse_filename(filename: &str) -> FileType {
    if let Some(captures) = SERIES_PARSER.captures(filename) {
        return FileType::SeriesEpisode(captures)
    }

    if let Some(captures) = MOVIE_PARSER.captures(filename) {
        return FileType::Movie(captures)
    }

    return FileType::Unknown
}

fn lint_file_name(file: &DirEntry, filename: &str, episode_name: bool) -> ComplianceStatus {
    if let Some(ext) = file.path().extension() {
        if !VIDEO_EXTENSIONS.contains(&ext.to_str().unwrap()) {
            return ComplianceStatus::Ignored;
        }
    } else {
        return ComplianceStatus::Ignored;
    }

    match parse_filename(filename) {
        FileType::SeriesEpisode(captures) => {
            if episode_name {
                check_rules(SERIES_RULES_WITH_NAME, DetectedFileType::SeriesEpisode, filename, &captures)
            } else {
                check_rules(SERIES_RULES_WITHOUT_NAME, DetectedFileType::SeriesEpisode, filename, &captures)
            }
        }
        FileType::Movie(captures) => check_rules(MOVIE_RULES, DetectedFileType::Movie, filename, &captures),
        FileType::Unknown => ComplianceStatus::NotMatched
    }
}

fn check_rules<const N: usize>(validation_rules: [fn(&str, &Captures) -> Option<NonCompliantReason>; N], file_type: DetectedFileType, filename: &str, captures: &Captures) -> ComplianceStatus {
    for rule in validation_rules {
        if let Some(reason) = rule(filename, &captures) {
            return ComplianceStatus::NonCompliant(file_type, reason);
        }
    }

    ComplianceStatus::Compliant(file_type)
}