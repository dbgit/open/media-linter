use crate::DetectedFileType;

pub enum StatusIcon {
    Failure,
    Warning,
    Success,
    Directory,
}

pub fn get_status_icon(icon: StatusIcon, no_emoji: bool) -> String {
    match icon {
        StatusIcon::Failure => { if no_emoji { "F -".to_string() } else { "❌".to_string() } }
        StatusIcon::Warning => { if no_emoji { "W -".to_string() } else { "⚠️".to_string() } }
        StatusIcon::Success => { if no_emoji { "S -".to_string() } else { "✔️".to_string() } }
        StatusIcon::Directory => { if no_emoji { "".to_string() } else { "📂".to_string() } }
    }
}


pub enum MediaIcon {
    SeriesEpisode,
    Movie,
    Unknown,
}

impl From<DetectedFileType> for MediaIcon {
    fn from(d: DetectedFileType) -> Self {
        match d {
            DetectedFileType::SeriesEpisode => Self::SeriesEpisode,
            DetectedFileType::Movie => Self::Movie
        }
    }
}

pub fn get_media_icon(icon: MediaIcon, no_emoji: bool) -> String {
    match icon {
        MediaIcon::SeriesEpisode => { if no_emoji { "S -".to_string() } else { "🎞️".to_string() } }
        MediaIcon::Movie => { if no_emoji { "M -".to_string() } else { "🎬".to_string() } }
        MediaIcon::Unknown => { if no_emoji { "U -".to_string() } else { "❓".to_string() } }
    }
}