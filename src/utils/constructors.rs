pub fn episode_name(title: String, season: String, episode: String, name: Option<String>, format: String) -> String {
    match name {
        Some(n) => format!("{} S{}E{} - {}.{}", title, season, episode, n, format),
        None => format!("{} S{}E{}.{}", title, season, episode, format),
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn formats_with_name() {
        let expected_name = "Test S00E00 - FooBar.mkv".to_string();
        let result = super::episode_name(
            "Test".to_string(),
            "00".to_string(),
            "00".to_string(),
            Some("FooBar".to_string()),
            "mkv".to_string()
        );
        assert_eq!(expected_name, result);
    }

    #[test]
    fn formats_without_name() {
        let expected_name = "Test S00E00.mkv".to_string();
        let result = super::episode_name(
            "Test".to_string(),
            "00".to_string(),
            "00".to_string(),
            None,
            "mkv".to_string()
        );
        assert_eq!(expected_name, result);
    }
}
