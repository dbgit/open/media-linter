use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    pub path: String,

    #[clap(long, value_parser)]
    pub no_emoji: bool,

    #[clap(long, value_parser)]
    pub show_success: bool,

    #[clap(long, value_parser)]
    pub episode_name: bool,
}