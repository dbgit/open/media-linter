# Media Linter

A tool to lint the file names of your media

## Usage

```
USAGE:
    media-linter.exe [OPTIONS] <PATH>

ARGS:
    <PATH>

OPTIONS:
    -h, --help            Print help information
        --no-emoji
        --show-success
    -V, --version         Print version information
```

## Filenames

| Type  | Filename                             |
|-------|--------------------------------------|
| Show  | Show Name S00E00 - Episode Name.ext  |
| Movie | Movie Name \(year) \[resolution].ext |

## Rules

...
